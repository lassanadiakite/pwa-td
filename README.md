# Todo 

## TD

Réaliser une PWA Todo list avec les fonctionnalité suivantes:
* Ajouter une tache avec un titre et une description et un état réalisé
* Sauvegarder les todos en DB
* Créer/Lire les todos offline
* Synchroniser les todos créés/modifié hors ligne à la recupération de la connexion
* Bonus : Avoir une page de détail de todo



# pwa-td
# npm install
# npm run css
# npm run database
# npm run serve

